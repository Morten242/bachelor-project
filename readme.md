# VRterra #

## Description ##

This repository contains the irrlicht based C++ modules of VRterra:

* Projector Program
    * The projector program will automatically try to connect to a kinect plugged into the system, start a server that will pull terrain data from the kinect and host it, as well as project the terrain back out on the screen. When the program first starts the user is able to calibrate the projv ector program by clicking four times in a quadrilateral. This quadrilateral is then UV mapped and projected back onto the sand.
* First Person Demo
    * The first person demo has a camera that the player can use to fly around in the terrain, if connected to a server one can also fly around in real-time updated terrain.
* NoKinect
    * The NoKinect program is basically a projector program without the kinect running. It will load a basic terrain file already in the project.


## Build ##

This will cover requirements to build and how to build the project on different platforms.

### Requirements

```text
Irrlicht v1.8.3
```

Can be installed with your favorite package manager on Linux. On Windows you need to download and unzip the engine. After this you can add a environment variable called 'IRRLICHT_HOME' which points to the folder which contains the 'include' folder. CMake should then handle the rest.

#### Linux Specific ####

```text
libfreenect
libusb
```

Required for the Kinect application to function.

### Development ###

#### Building On Windows ####

Open a command line and navigate to the folder containing this project. Make sure cmake is in your path and then run:

```batch
> mkdir build
> cd build
# Generate a solution, replace '<Generator>' with your cmake generator of choice
> cmake -G <Generator> ..
# to build NoKinect:
> cmake --build . --target NoKinect --config release
# or to build the first person demo:
> cmake --build . --target irrlicht-game --config release
```

#### Building On GNU/Linux ####

1. Install the listed libraries in Requiremenst using your favorite package manager.
1. Navigate to the irrlicht folder (this repository) and make a build folder with `mkdir build`
1. Navigate to the build folder with `cd build` and run cmake with `cmake -G "Unix Makefiles" ..`
1. Build by running `make`

You can build all the projects on GNU/Linux

## How To Run ##

### Configurations ###

Configuration files are located in the `configuration` folder in the root directory of the project. The files in this folder belong to different applications or different parts of the application.

* `game.json` is read by irrlicht-game. HostAddress is the address of the server to connect to.
* `projector.json` is read by irrlicht-projector and NoKinect.
* `server.json` is read by the server which runs alongside irrlicht-projector and NoKinect. The settings in here likely do not need to be altered, but the port can be changed by altering ListenPort.

### Running On Windows ###

Unfortunately libfreenect and libusb does not on Windows as it does on Linux, this means that you will be unable to run the kinect server on Windows. But you can still run the first person demo and connect to a server running the kinect demo.

1. Follow the instructions above to build irrlicht-game
1. Set up the configuration to be as you want them to
1. Navigate to the root folder using a command line
1. run `.\bin\Release\irrlicht-game.exe`

### Running On GNU/Linux ###

1. Navigate to the bin folder, make sure the configuration files are with the binaries as well and run the binary, for example like this: `./irrlicht-projector`
1. Optionally one can start it in the above folder, next to the configurations folder by running something like this: `./bin/irrlicht-projector`

## TCP API ##

Can be found [here](api.md).