#version 330 compatibility

in vec4 position;

void main(void)
{
    
    vec4 color = vec4(0, 1, 0, 0);
    if (position.y > 500)
    {
        color += vec4(1, -1, 0, 0);
    }
    // set color
    gl_FragColor = gl_Color * color;
}