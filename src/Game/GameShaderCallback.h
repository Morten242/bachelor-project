﻿#pragma once
#include <Base/ShaderCallback.h>

#include <irrlicht.h>

/**
* Made for use with the irrlicht terrain, handles the callbacks that irrlicht generated for it
*
* @author Mårten Nordheim
*/
class GameShaderCallback : public ShaderCallback
{
public:
    /**
    * Constructor for GameShaderCallback
    *
    * @param device a pointer to the irrlicht device of this application
    */
    explicit GameShaderCallback(irr::IrrlichtDevice* device) : ShaderCallback(device)
    {
    }

    /**
    * Destructor for GameShaderCallback
    */
    ~GameShaderCallback()
    {
    }

    /**
    * Callback which Irrlicht uses
    *
    * Sends in necessary constants to produce simple lighting
    */
    void OnSetConstants(irr::video::IMaterialRendererServices* services, irr::s32 userData) override
    {
        auto driver = services->getVideoDriver();

        auto inverseWorld = driver->getTransform(irr::video::ETS_WORLD);
        inverseWorld.makeInverse();
        services->setVertexShaderConstant("inverseWorld", inverseWorld.pointer(), 16);

        auto worldViewProj = driver->getTransform(irr::video::ETS_PROJECTION);
        worldViewProj *= driver->getTransform(irr::video::ETS_VIEW);
        worldViewProj *= driver->getTransform(irr::video::ETS_WORLD);
        services->setVertexShaderConstant("worldViewProj", worldViewProj.pointer(), 16);

        services->setVertexShaderConstant("lightPosition",
                                          reinterpret_cast<irr::f32*>(&m_lightPosition),
                                          3);

        auto transposedWorld = driver->getTransform(irr::video::ETS_WORLD).getTransposed();
        services->setVertexShaderConstant("transposedWorld", transposedWorld.pointer(), 16);
    }

    void setLightPosition(irr::core::vector3df lightPosition)
    {
        m_lightPosition = lightPosition;
    }

private:
    irr::core::vector3df m_lightPosition;
};