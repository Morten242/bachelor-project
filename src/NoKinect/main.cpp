#include <Base/Base.h>
#include <Base/BaseReceiver.h>
#include <Server/Server.h>
#include <Terrain/Terrain.h>

#include <Base/Projector.h>
#include <string>
#include <thread>

void updateTerrainTest(Terrain* terrain, bool* run)
{
    std::vector<float> buf(640 * 480);
    std::fill(buf.begin(), buf.end(), 0.f);
    terrain->copyFromBuffer(buf);

    while (*run)
    {
        std::for_each(buf.begin(), buf.end(), [](float& f) {
            if (rand() % 30 == 0)
                f += 10;
        });
        terrain->copyFromBuffer(buf);
    }
}

int main(int, char**)
{
    auto terrain = std::make_shared<Terrain>(640, 480);
    auto run     = true;

    Server server;
    server.initialize(terrain);

    terrain->addCallbackFunction("server", [&server](size_t, float, float, bool lastUpdate) {
        if (lastUpdate)
        {
            server.setTerrainUpdated();
        }
    });

    server.start();
    server.startTcpServer();

    Projector projector{"configuration/projector.json", terrain.get()};

    auto receiver =
        new BaseReceiver(projector.getTerrain(), projector.getDevice(), projector.getMouse());

    projector.setReceiver(receiver);

    auto test = std::thread(updateTerrainTest, terrain.get(), &run);

    projector.run();

    run = false;
    server.stop();
    test.join();

    return 0;
}
