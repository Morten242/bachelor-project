file(GLOB SRC_FILES "*.h" "*.cpp")

add_executable(NoKinect ${SRC_FILES})

target_link_libraries(NoKinect Base Server)
install(TARGETS NoKinect RUNTIME DESTINATION ${BIN_DIR})