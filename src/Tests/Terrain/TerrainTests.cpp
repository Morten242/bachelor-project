#include <gtest/gtest.h>

#include <Terrain/Terrain.h>

TEST(TerrainTests, TestResizeTerrain)
{
    Terrain terr {2,2};
    ASSERT_EQ(4, terr.getSize());
    ASSERT_EQ(2, terr.getWidth());
    ASSERT_EQ(2, terr.getHeight());
    
    terr.setNewSize(10,10);
    ASSERT_EQ(100, terr.getSize());
    ASSERT_EQ(10, terr.getWidth());
    ASSERT_EQ(10, terr.getHeight());

    terr.setNewSize(3, 6);
    ASSERT_EQ(18, terr.getSize());
    ASSERT_EQ(3, terr.getWidth());
    ASSERT_EQ(6, terr.getHeight());
}