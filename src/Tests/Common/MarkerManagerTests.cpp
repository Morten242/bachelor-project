#include <gtest/gtest.h>

#include <Common/MarkerManager.h>

TEST(MarkerManagerTests, TestCreateNewMarker)
{
    auto man = MarkerManager::getInstance();
    auto id = man->createNewMarker();
    ASSERT_EQ(0, id);
    id = man->createNewMarker();
    ASSERT_EQ(1, id);
}

TEST(MarkerManagerTests, TestGetMarker)
{
    auto man = MarkerManager::getInstance();
    auto m = man->getMarker(9999);
    ASSERT_EQ(nullptr, m);
    
    auto id = man->createNewMarker();
    m = man->getMarker(id);
    ASSERT_NE(nullptr, m);
    ASSERT_EQ(id, m->id);
    
    m->x = m->y = 42;
    
    auto n = man->getMarker(id);
    ASSERT_EQ(42, n->x);
    ASSERT_EQ(42, n->y);
}
