#pragma once
#include "Base.h"

class Projector : public Base
{
public:
    /**
    * Constructor for Projector
    *
    * @param file the configuration file to use
    * @param ourTerrain a pointer to an instance of the Terrain class
    */
    Projector(const gsl::cstring_span<>& file, Terrain* ourTerrain);

    /**
    * Get pointer to mouse position (updated by receiver)
    * @return the pointer to mouse position
    */
    std::vector<irr::core::position2di>* getMouse();

protected:
    /**
    * Set up the camera to use in the scene
    */
    void setupIrrlichtCamera();

    /**
    * Update Projector-specific elements
    *
    * @param deltaTime the time taken to complete one iteration of the game loop
    */
    void update(float deltaTime) override;

    /**
    * A chance to draw to the screen before it is displayed
    */
    void derivedDraw() override;

    /**
    * Draw lines between the currently selected points
    */
    void drawSelectionSquare() const;

    /**
    * Force the terrain to be redrawn instead of using the seemingly cached version
    *
    * @param terrain a pointer to irrlicht terrain scene node
    */
    void derivedTerrainUpdated(irr::scene::ITerrainSceneNode* terrain) override;

private:
    /**
    * Render to the texture which will be displayed
    */
    void renderToTexture();

    /**
    * Map the texture to the surface so that we get the selection chosen
    */
    void mapTexture();

    /**
    * Create the mesh buffer and mesh node used to display a selected portion of the sandbox
    */
    void initializeSelectionDisplayMesh();

    /**
    * Update the markers's position
    */
    void updateMarkers();

    // Camera specific fields
    int m_projectionMatrixWidth  = 9600;
    int m_projectionMatrixHeight = 7200;

    // sandbox-selection-specific fields
    std::vector<irr::core::position2di> m_mousePositions;
    irr::video::ITexture* m_renderTexture;
    irr::scene::SMeshBuffer* m_meshBuffer;
    irr::scene::IMeshSceneNode* m_meshNode;
    bool m_textureWasMapped;

    //// Marker-specific fields
    // keeps track of all markers on the network
    MarkerManager* m_markerManager;
    // Keeps track of the markers being drawn on the sand
    std::vector<irr::gui::IGUIImage*> m_markers;
    // texture for the markers
    irr::video::ITexture* m_markerTexture;
};
