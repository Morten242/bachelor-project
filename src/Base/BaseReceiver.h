#pragma once

#include <Terrain/Terrain.h>

#include <irrlicht.h>

/**
 * Base event receiver class for receiving and processing events
 * @author Nichlas Severinsen
 */
class BaseReceiver : public irr::IEventReceiver
{
public:
    /**
     * Constructor
     *
     * @param terrain pointer to a terrain
     * @param device a pointer to the device running in this application
     * @param mouse pointer to a vector which will be occupied with positions of mouse clicks
     */
    BaseReceiver(irr::scene::ITerrainSceneNode* terrain,
                 irr::IrrlichtDevice* device,
                 std::vector<irr::core::position2di>* mouse);

    /**
     * OnEvent function
     *
     * @param event event received
     */
    virtual bool OnEvent(const irr::SEvent& event) override;

private:
    irr::scene::ITerrainSceneNode* m_terrain;
    irr::IrrlichtDevice* m_device;
    std::vector<irr::core::position2di>* m_mousePositions;
};
