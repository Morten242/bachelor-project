#define NOMINMAX

#include "Base.h"

#include "ShaderCallback.h"

#include <Client/Client.h>
#include <Common/MarkerManager.h>
#include <Terrain/Terrain.h>

#include <algorithm>
#include <cmath>
#include <functional>

Base::Base(gsl::cstring_span<> file, Terrain* ourTerrain)
    : m_camera(nullptr),
      m_device(nullptr),
      m_driver(nullptr),
      m_smgr(nullptr),
      m_guienv(nullptr),
      m_config(nullptr),
      m_ourTerrain(ourTerrain)
{
    m_config = new Utils::Config();
    m_config->load(file);

    initializeIrrlichtWindow();

    m_driver = m_device->getVideoDriver();
    m_smgr   = m_device->getSceneManager();
    m_guienv = m_device->getGUIEnvironment();
}

Base::~Base()
{
    m_device->drop();
}

void Base::run()
{
    auto then = m_device->getTimer()->getTime();
    while (m_device->run())
    {
        auto now       = m_device->getTimer()->getTime();
        auto deltaTime = static_cast<float>(now - then) / 1000.f;
        then           = now;

        update(deltaTime);

        m_driver->beginScene();

        // mutex lock scope
        {
            std::lock_guard<std::mutex> lock(m_renderMutex);
            m_smgr->drawAll();
        }
        m_guienv->drawAll();

        derivedDraw();

        m_driver->endScene();
    }
}

int Base::loadShader(ShaderCallback* shaderCallback) const
{
    // based on code from http://irrlicht.sourceforge.net/docu/example010.html
    auto customMaterialType = -1;
    auto config             = m_config;
    irr::io::path vsFile    = config->getValue<std::string>("VertexShader").c_str();
    irr::io::path psFile    = config->getValue<std::string>("FragmentShader").c_str();
    auto gpu                = m_driver->getGPUProgrammingServices();
    if (gpu != nullptr)
    {
        customMaterialType = gpu->addHighLevelShaderMaterialFromFiles(vsFile,
                                                                      "vertexMain",
                                                                      irr::video::EVST_VS_3_0,
                                                                      psFile,
                                                                      "pixelMain",
                                                                      irr::video::EPST_PS_3_0,
                                                                      shaderCallback);
        shaderCallback->drop();
    }

    if (customMaterialType == -1)
    {
        FATAL("customMaterialType was invalid, check if paths are valid:" << std::endl
                                                                          << "vertex shader path:  "
                                                                          << vsFile.c_str()
                                                                          << std::endl
                                                                          << "pixel shader path:  "
                                                                          << psFile.c_str());
    }

    return customMaterialType;
}

void Base::initializeIrrlichtWindow()
{
    m_screenDim.Width  = m_config->getValue<unsigned>("WindowWidth");
    m_screenDim.Height = m_config->getValue<unsigned>("WindowHeight");

    m_device = irr::createDevice(irr::video::EDT_OPENGL,
                                 m_screenDim,
                                 16,
                                 m_config->getValue<bool>("Fullscreen"),
                                 m_config->getValue<bool>("Vsync"),
                                 false,
                                 nullptr);
    auto str_title = m_config->getValue<std::string>("WindowTitle");
    auto title     = std::wstring(str_title.begin(), str_title.end());  // damn wstrings
    m_device->setWindowCaption(title.c_str());
}

void Base::setupIrrlichtTerrain(ShaderCallback* shaderCallback)
{
    m_terrain = m_smgr->addTerrainSceneNode("res/terrain-heightmap.bmp",
                                            nullptr,
                                            -1,
                                            irr::core::vector3df(0.f, 0.f, 0.f),
                                            irr::core::vector3df(0.f, 0.f, 0.f),
                                            irr::core::vector3df(30.f, 1.0f, 40.f),
                                            irr::video::SColor(255, 255, 255, 255),
                                            1,
                                            irr::scene::ETPS_17,
                                            4);
    auto customMaterialType = loadShader(shaderCallback);
    m_terrain->setMaterialType(static_cast<irr::video::E_MATERIAL_TYPE>(customMaterialType));
    m_terrain->setMaterialFlag(irr::video::EMF_NORMALIZE_NORMALS, true);

    // Update the irrlicht terrain when the Terrain-library gets updated
    auto func = [this](size_t index, float newVal, float, bool isFinalUpdate) {
        // 256 is the width (and height) of the heightmap we load in which generates the terrain
        this->syncMeshWithTerrainUpdates(256, index, newVal, isFinalUpdate);
    };
    m_ourTerrain->addCallbackFunction("irrlicht_terrain", func);
    m_terrain->setScale(m_terrain->getScale());
    m_terrain->updateAbsolutePosition();
}

irr::core::dimension2du Base::getScreenDimensions() const
{
    return m_screenDim;
}

void Base::setReceiver(irr::IEventReceiver* receiver) const
{
    m_device->setEventReceiver(receiver);
}

irr::scene::ITerrainSceneNode* Base::getTerrain() const
{
    return m_terrain;
}

irr::scene::ICameraSceneNode* Base::getCamera() const
{
    return m_camera;
}

irr::IrrlichtDevice* Base::getDevice() const
{
    return m_device;
}

void Base::syncMeshWithTerrainUpdates(size_t meshSize,
                                      size_t index,
                                      float newVal,
                                      bool isFinalUpdate)
{
    auto width  = m_ourTerrain->getWidth();
    auto mesh   = m_terrain->getMesh();
    auto xRatio = static_cast<float>(m_ourTerrain->getWidth()) / meshSize;
    auto zRatio = static_cast<float>(m_ourTerrain->getHeight()) / meshSize;

    auto x = static_cast<size_t>((index % width) / xRatio);
    // Need to cover entire distance between x and x + 1
    auto xMax = static_cast<size_t>(((index % width) + 1) / xRatio);

    auto z = static_cast<size_t>((index / width) / zRatio);
    // Need to cover entire distance between z and z + 1
    auto zMax = static_cast<size_t>(((index / width) + 1) / zRatio);

    // complexity, ouch
    for (auto currX = x; currX < xMax; currX++)
    {
        for (auto currZ = z; currZ < zMax; currZ++)
        {
            auto scaledInd = static_cast<size_t>(currX + meshSize * currZ);
            Expects(scaledInd < meshSize * meshSize);
            for (irr::u32 i = 0; i < mesh->getMeshBufferCount(); i++)
            {
                auto meshBuffer = mesh->getMeshBuffer(i);

                if (meshBuffer->getVertexType() != irr::video::EVT_2TCOORDS)
                {
                    continue;
                }

                auto vertices =
                    static_cast<irr::video::S3DVertex2TCoords*>(meshBuffer->getVertices());
                vertices[scaledInd].Pos.Y = newVal;
            }
        }
    }

    if (isFinalUpdate)
    {
        // There are slightly different ways that the terrain update will be handled, so the derived
        // classes can handle it
        derivedTerrainUpdated(m_terrain);
    }
}

irr::scene::ISceneManager* Base::getSceneManager() const
{
    return m_smgr;
}

Utils::Config* Base::getConfig() const
{
    return m_config;
}

irr::video::IVideoDriver* Base::getDriver() const
{
    return m_driver;
}

Terrain* Base::getTerrainClassInstance() const
{
    return m_ourTerrain;
}
