﻿#pragma once
#include "Error.h"

#include <gsl.h>

#include <fstream>
#include <string>

/****************************
*
* This file contains some free function(s) relating to IO
*
* @author Mårten Nordheim
*
****************************/

namespace Utils
{
    /**
    * Read a file's content in to a string
    * @param path the path to the file
    */
    inline std::string fileToString(const gsl::cstring_span<>& path)
    {
        std::ifstream file(path.data());
        if (!file)
        {
            FATAL("Failed when attempting to open file: " << path.data());
        }

        // Read the file
        std::string content;
        content.assign(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>());
        return content;
    }
}
