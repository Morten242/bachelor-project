#pragma once
#include <sstream>
#include <string>
#include <string_span.h>

// No need to check for '_DEBUG' *AND* 'DEBUG' when checking if we're in debug mode
#if defined(_DEBUG) && !defined(DEBUG)
#define DEBUG 1
#endif

namespace Utils
{
    /**
    * Prints a filename, line and string to the screen.
    * Note: You should use one of the macros to access this
    * @param file the filename related to the message
    * @param line the line at which the logger was called
    * @param str The string to print
    */
    extern void logToScreen(std::string file, int line, std::string str);

#ifdef DEBUG
    // Define a debug macro (writes to screen and file)
    // Note: The DBG macro only works when DEBUG is defined
#define DBG(x)                                                                                     \
    do                                                                                             \
    {                                                                                              \
        std::stringstream SS;                                                                      \
        SS << x;                                                                                   \
        Utils::logToScreen(__FILE__, __LINE__, SS.str());                                         \
    } while (0)
#else
#define DBG(x)
#endif

    // Fatal error - log a message and exit
#define FATAL(x)                                                                                   \
    do                                                                                             \
    {                                                                                              \
        std::stringstream SS;                                                                      \
        SS << x;                                                                                   \
        Utils::logToScreen(__FILE__, __LINE__, SS.str());                                         \
        exit(-1);                                                                                  \
    } while (0)
}
