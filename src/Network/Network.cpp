#include "Network.h"

#include "NetworkUtils.h"
#include <Utils/Container.h>
#include <Utils/Error.h>

Network::Network() : m_mode(Mode::None), m_localEndPoint(nullptr), m_running(false)
{
}

Network::~Network()
{
    if (m_localEndPoint != nullptr)
    {
        enet_host_destroy(m_localEndPoint);
    }
}

void Network::stop()
{
    m_running = false;
}

void Network::initialize()
{
    if (enet_initialize() != 0)
    {
        FATAL("An error occurred while initializing ENet.");
    }
    atexit(enet_deinitialize);
}

void Network::startServer(uint16_t port)
{
    if (m_localEndPoint != nullptr)
    {
        FATAL("Already running!");
    }

    ENetAddress address;

    address.host = ENET_HOST_ANY;
    address.port = port;

    m_localEndPoint = enet_host_create(&address, 32, 2, 0, 0);

    if (m_localEndPoint == nullptr)
    {
        FATAL("An error occurred while trying to create an ENet server host.");
    }

    m_running = true;
    DBG("Started server on port " << PORT_NUMBER);

    m_mode = Mode::Server;
}

void Network::startClient()
{
    if (m_localEndPoint != nullptr)
    {
        FATAL("Already running");
    }

    m_localEndPoint = enet_host_create(nullptr, 1, 2, 0, 0);

    if (m_localEndPoint == nullptr)
    {
        FATAL("An error occurred while trying to create an ENet client host.");
    }

    m_running = true;
    DBG("Started client");

    m_mode = Mode::Client;
}

bool Network::connect(const gsl::cstring_span<> serverAddress, ENetPeer** outPeer)
{
    if (m_mode != Mode::Client)
    {
        FATAL("Need to start the client first!");
    }
    *outPeer = nullptr;
    ENetEvent event{};

    ENetAddress address;
    enet_address_set_host(&address, serverAddress.data());
    address.port = PORT_NUMBER;
    m_peers.push_back(enet_host_connect(m_localEndPoint, &address, 2, 0));

    if (m_peers[m_peers.size() - 1] == nullptr)
    {
        DBG("Connection to " << serverAddress.data() << ":" << PORT_NUMBER << " failed" << std::endl
                             << "No available peers for initiating an ENet connection.");
        return false;
    }

    // Wait for a reply or time out and fail
    if (enet_host_service(m_localEndPoint, &event, CONNECTION_WAIT_MS) > 0 &&
        event.type == ENET_EVENT_TYPE_CONNECT)
    {
        DBG("Connection to " << serverAddress.data() << ":" << PORT_NUMBER << " succeeded");
        *outPeer = event.peer;
        return true;
    }

    // If we get here then we timed out, was rejected or otherwise failed to connected:
    enet_peer_reset(m_peers[m_peers.size() - 1]);
    m_peers.erase(m_peers.end() - 1);

    DBG("Connection to " << serverAddress.data() << ":" << PORT_NUMBER << " failed");
    return false;
}

void Network::shutdown()
{
    if (m_localEndPoint == nullptr)
    {
        return;
    }

    Expects(!m_running);

    for (const auto& peer : m_peers)
    {
        auto disconnectSuccessful = false;
        ENetEvent event{};
        enet_peer_disconnect(peer, 0);

        // Loop until disconnected or until something fails (timeout = 3000ms)
        while (!disconnectSuccessful && enet_host_service(m_localEndPoint, &event, 3000) > 0)
        {
            switch (event.type)
            {
            case ENET_EVENT_TYPE_RECEIVE:
            {
                // Need to deallocate any incoming packet that isn't going to be processed since
                // we're disconnecting...
                enet_packet_destroy(event.packet);
                break;
            }
            case ENET_EVENT_TYPE_DISCONNECT:
            {
                disconnectSuccessful = true;
                DBG("Successfully disconnected from "
                    << NetUtils::getIPString(event.peer->address));
                break;
            }
            case ENET_EVENT_TYPE_CONNECT:
            {
                // Someone connected to us while we're shutting down, force disconnection:
                enet_peer_disconnect_now(event.peer, 0);
                break;
            }
            case ENET_EVENT_TYPE_NONE:
                break;
            default:
                break;
            }
        }

        // If we did not properly disconnect we need to force a disconnection
        if (!disconnectSuccessful)
        {
            enet_peer_reset(peer);
        }
    }
    m_peers.clear();
    enet_host_destroy(m_localEndPoint);
    m_localEndPoint = nullptr;
}

gsl::owner<ENetEvent*> Network::receivePacket(uint32_t waitTime)
{
    auto event = new ENetEvent;

    if (enet_host_service(m_localEndPoint, event, waitTime) > 0)
    {
        switch (event->type)
        {
        case ENET_EVENT_TYPE_CONNECT:
        {
            DBG("A new client connected from " << NetUtils::getIPString(event->peer->address) << ":"
                                               << event->peer->address.port);

            event->channelID = 0;
            m_peers.push_back(event->peer);
            break;
        }

        case ENET_EVENT_TYPE_RECEIVE:
        {
            // Return the event (need the event so we can retrieve information _around_ the packet
            // as well)
            return event;
        }

        case ENET_EVENT_TYPE_DISCONNECT:
        {
            DBG(NetUtils::getIPString(event->peer->address) << " disconnected!");
            Utils::remove(&m_peers, event->peer);
            return event;
        }
        case ENET_EVENT_TYPE_NONE:
        {
            break;
        }
        }
    }

    delete event;
    return nullptr;
}

bool Network::isRunning() const
{
    return m_running;
}

bool Network::isClientConnected() const
{
    Expects(m_mode == Mode::Client);
    // The client will only have 1 peer (the server)
    return m_peers.size() == 1;
}

void Network::sendPacket(gsl::owner<ENetPacket*> packet, const gsl::not_null<ENetPeer*>& peer) const
{
    Expects(peer != nullptr);
    enet_peer_send(peer, 0, packet);
    enet_host_flush(m_localEndPoint);
}

void Network::sendData(const gsl::span<uint8_t>& dataSpan,
                       const gsl::not_null<ENetPeer*>& peer) const
{
    Expects(peer != nullptr);
    auto packet =
        enet_packet_create(dataSpan.data(), dataSpan.size_bytes(), ENET_PACKET_FLAG_RELIABLE);
    if (packet == nullptr)
    {
        DBG("Packet could not be created!");
        return;
    }
    if (enet_peer_send(peer, 0, packet) != 0)
    {
        DBG("Packet was not sent!");
    }
    enet_host_flush(m_localEndPoint);
}