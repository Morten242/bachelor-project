#pragma once
#include <enet/enet.h>
#include <gsl.h>

#include <vector>

static const int PORT_NUMBER        = 24242;
static const int CONNECTION_WAIT_MS = 5000;

/**
* Wrapper around enet for use in networking
*
* @author Mårten Nordheim
*/
class Network
{
public:
    /**
     * Constructor
     */
    Network();

    /**
     * Destructor
     */
    ~Network();

    /**
     * No need for a move constructor
     */
    Network(Network&& o) = delete;

    /**
    * Essentially just sets 'isRunning' to return false
    */
    void stop();

    /**
    * Initialize enet
    */
    static void initialize();

    /**
     * Creates a server, which can be connected to by clients
     * @param port the port to listen on
     */
    void startServer(uint16_t port);

    /**
     * Creates a client that is able to connect to a server and send packets.
     */
    void startClient();

    /**
     * Sends a packet, currently this function sends a predefined packet as an example.
     * @param packet a pointer to the packet
     * @param peer the peer to send it to
     */
    void sendPacket(gsl::owner<ENetPacket*> packet, const gsl::not_null<ENetPeer*>& peer) const;

    /**
    * Send some data to a peer
    * @param dataSpan a container containing some data
    * @param peer the peer to send the data to
    */
    void sendData(const gsl::span<uint8_t>& dataSpan, const gsl::not_null<ENetPeer*>& peer) const;

    /**
     * Connects to a server
     * @param serverAddress IP address of the server to connect to.
     * @param outPeer the peer you connected to will be assigned here
     * @return true on success
     */
    bool connect(const gsl::cstring_span<> serverAddress, ENetPeer** outPeer);

    /**
     * Disconnects from peer(s) and shuts down the connection
     * A client can only be connected to one server at a time, this function disconnects
     * if from that server.
     * If called on a server then it will disconnect from all connected clients
     */
    void shutdown();

    /**
    * Check for incoming packets. If a packet was retrieved then the _event_ will be returned.
    * This is done since it's the only way to get access to the host and various other info.
    * @param waitTime time you're willing to wait for a packet to arrive
    * @return the event if a packet was received, nullptr in any other case
    */
    gsl::owner<ENetEvent*> receivePacket(uint32_t waitTime = 0);

    /**
    * Check if the network connection is still operational
    * @return true if the network connection is still going, false otherwise
    */
    bool isRunning() const;

    /**
    * Is connected to anything (client only)
    * @return true if connected to anything
    */
    bool isClientConnected() const;

private:

    enum class Mode
    {
        None,
        Server,
        Client
    };

    Mode m_mode;
    ENetHost* m_localEndPoint;
    std::vector<ENetPeer*> m_peers;
    bool m_running;
};