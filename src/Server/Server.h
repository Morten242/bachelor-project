﻿#pragma once
#include <Common/MarkerManager.h>
#include <Network/Flags.h>
#include <Network/Network.h>
#include <Terrain/Terrain.h>

#define ASIO_STANDALONE
#include <asio.hpp>

#include <json.hpp>
#include <thread>
#include <vector>

/**
* Class for the server which serves the terrain data
*
* @author Mårten Nordheim
*/
class Server
{
public:
    /**
    * Default constructor for Server
    */
    Server();

    /**
    * Default destructor for Server
    */
    ~Server();

    /**
    * Initialize, set up and load everything needed for the server
    */
    void initialize(std::shared_ptr<Terrain> terrain = nullptr);

    /**
    * Start the Server
    *
    * @see #stop
    */
    void start();

    /**
    * Stop the server and the tcp server if it is running
    *
    * @see #start
    */
    void stop();

    /**
    * Start the tcp server, runs in a separate thread
    *
    * @see #stop
    */
    void startTcpServer();

    /**
    * Call this when the terrain has been updated, will be broadcasted soon after
    */
    void setTerrainUpdated();

private:
    /**
    * TCP server, listens for connections and responds with the heightmap
    *
    * @author Mårten Nordheim
    */
    class TcpServer
    {
    public:
        /**
        * Constructor for TcpServer
        *
        * @param io_service an instance of asio::io_service
        * @param port the port to listen on
        * @param terrain pointer to the terrain
        */
        TcpServer(asio::io_service& io_service, uint16_t port, std::shared_ptr<Terrain> terrain);

        /**
        * Tell the server to stop running
        */
        void stop();

        /**
        * Check if the tcp server is running
        *
        * @return true if running or false if currently stopping or has stopped
        */
        static bool isRunning();

    private:
        /**
        * Accept a connection asynchronously
        */
        void doAccept();

        /**
        * Check an asio error code and see if it's possible to continue running
        *
        * @param error the error received
        * @param bytesRead the amount of bytesRead during the operation which triggered the error
        * @return true if it is possible to keep going, false otherwise
        */
        static bool checkAsioError(asio::error_code& error, const size_t& bytesRead = 0);

        /**
        * Read from a socket until a certain size has been reached or until it is unable to read any
        * more
        *
        * @param socket the socket to read from
        * @param streamBuffer the buffer to read into
        * @param isConnected a pointer to a bool which says whether or not the socket is connected,
        * will get updated inside this function if it gets disconnected
        * @param requiredSize the size which the streamBuffer must be equal or greater than
        * @return any error code which forces it to cancel and exit early
        */
        static asio::error_code asioReadUntilSize(asio::ip::tcp::socket& socket,
                                                  asio::streambuf& streamBuffer,
                                                  bool* isConnected,
                                                  const size_t& requiredSize);
        /**
        * Read a full message from a socket
        *
        * @param socket the socket to read from
        * @param streamBuffer the buffer to read into, should be persistent between calls to avoid
        * loss of data
        * @param output a pointer to an initialized string
        * @param isConnected a pointer to a bool which says whether or not the socket is connected,
        * will get updated inside this function if it gets disconnected
        * @return any error code which forces it to cancel and exit early
        */
        static asio::error_code readAsioMessage(asio::ip::tcp::socket& socket,
                                                asio::streambuf& streamBuffer,
                                                std::string* output,
                                                bool* isConnected);

        /**
        * Send a json-encoded message through a connected socket
        *
        * @param socket the socket to send through
        * @param message the json message to send
        * @param error an instance of asio::error_code, any error will be assigned to it
        */
        static void sendJsonMessage(asio::ip::tcp::socket& socket,
                                    const nlohmann::json& message,
                                    asio::error_code& error);

        /**
        * Handle a tcp packet and respond to it
        *
        * @param socket the socket to respond on if necessary
        * @param incomingMessage the json-encoded message that was received
        * @param terrain a pointer to an instance of Terrain
        * @param isConnected a pointer to an instance of a boolean, will be set to false if the
        * connection can no longer continue
        * @param error any asio error encountered in this function will be assigned here
        */
        static void handleTcpMessage(asio::ip::tcp::socket& socket,
                                     const nlohmann::json& incomingMessage,
                                     Terrain* terrain,
                                     bool* isConnected,
                                     asio::error_code& error);

        /**
        * Function which handles exactly 1 connection
        *
        * @param terrain a shared ptr to the 'global' terrain
        * @param socket the socket/connection to handle
        */
        static void handleConnection(std::shared_ptr<Terrain> terrain,
                                     asio::ip::tcp::socket socket);

        // Are we supposed to keep running? true / false
        static bool m_running;
        // Asio objects..
        asio::ip::tcp::acceptor m_acceptor;
        asio::ip::tcp::socket m_socket;
        // vector of threads which are running
        std::vector<std::thread> m_tcpClientThreads;
        // Pointer to the terrain
        std::shared_ptr<Terrain> m_terrain;
        static MarkerManager* m_markerMan;
    };

    /**
    * Stops the tcp server
    *
    * @see #startTcpServer
    */
    void stopTcpServer();

    /**
    * Listens for incoming packets in a loop
    */
    void listenLoop();

    /**
    * Broadcast the terrain to all currently connected clients
    */
    void broadcastTerrain();

    Network m_network;
    std::shared_ptr<Terrain> m_terrain;
    uint16_t m_port;
    uint32_t m_waitTime;

    bool m_terrainHasUpdated;
    float m_maxTerrainBroadcastFreq;
    float m_broadcastTimer;

    std::vector<ENetPeer*> m_clients;
    std::mutex m_clientsLock;

    std::thread* m_thread;

    //// TCP-related fields:

    asio::io_service m_ioService;
    std::thread* m_tcpThread;
    TcpServer* m_tcpServerInstance;
};
