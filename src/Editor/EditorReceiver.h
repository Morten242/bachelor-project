#pragma once

#include <irrlicht.h>

class EditorReceiver : public irr::IEventReceiver
{
public:
    /**
     * Event handler
     * @param event event that comes in
     */
    virtual bool OnEvent(const irr::SEvent& event);

    /**
     * Function for returning which key is down
     * @param keyCode code for key that is down
     */
    virtual bool IsKeyDown(irr::EKEY_CODE keyCode) const
    {
        return m_KeyIsDown[keyCode];
    }

    /**
     * Constructor for EditorReceiver
     */
    EditorReceiver();

private:
    bool m_KeyIsDown[irr::KEY_KEY_CODES_COUNT];
};
