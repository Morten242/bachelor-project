# Professional Programming #

## Bachelor Project Report ##

We wrote a little bit for the professional programming course in Appendix K of our bachelor's report. It contains some code which we refactored as well as evaluation of professional tools that we looked at.

## General High Quality Code ##


It's hard to pick favorites, but here is one example of code which we would say is high-quality.

### NetUtils.. or: My code is the best  (Mårten Nordheim) ##

I (Mårten) think the free functions in the NetUtils namespace are small, very clean and powerful. This is probably because the functions themselves are stateless (the objects they interact with are certainly not) and they should, theoretically, always perform the same. They were very useful in writing the ENet portion of the networking and did not really receive many changes or fixes.

[https://bitbucket.org/vrterra/irrlicht/src/master/src/Network/NetworkUtils.h#cl-18](https://bitbucket.org/vrterra/irrlicht/src/master/src/Network/NetworkUtils.h#cl-18)

## Hacky Code ##

It's easy to pick out things you don't like. So here are some pieces of code which we're not going to stick on the fridge.

### How do you update the mesh and normals, again? ###

The terrain we used in Irrlicht does indeed have support for updating its mesh (and write changes to the buffers) as well as updating normals. Because we were using a generic terrain interface we did not have direct access to the actual implementation of it. This meant that, even though it supported doing these things, we could not do them directly without downcasting (which is a no-no.) By looking through forum posts we realized that the changes made to the mesh would only be reflected when the terrain updated its buffers, which was triggered when you moved its position. Which means that if you tell it to move, but don't actually move it anywhere, you get the job done; `terrain.setPosition(terrain.getPosition())`

Next up we had an issue with lighting in the simple FPS demo we did - the normals weren't updating along with the mesh! We found no solution on how to do this in the forum, so Mårten opened up the implementation of the terrain we were using to see if any functions which the interface exposed would update the normals. He found one (there are possibly more, but one is all we need); setScale. This means that to update the normals we would have to tell it to change its scale. Or we could pretend: `terrain.setScale(terrain.getScale())`.

If we were to optimize and make this less hacky we would have gone into the engine to expose these much-needed functions instead of relying on them being called as part of a chain.

[https://bitbucket.org/vrterra/irrlicht/src/master/src/Base/Projector.cpp#cl-253](https://bitbucket.org/vrterra/irrlicht/src/master/src/Base/Projector.cpp#cl-253)

One extra addition we needed to perform in the FPS demo was to update the camera collision. The camera's collision would not update when the mesh updated, which was strange. After looking around on the forum we realized that to update the collision we had to recreate the 'collision animator' *every time* we updated the mesh.

[https://bitbucket.org/vrterra/irrlicht/src/master/src/Game/Game.cpp#cl-94](https://bitbucket.org/vrterra/irrlicht/src/master/src/Game/Game.cpp#cl-94)

### Shutting down the TCP listener (Mårten Nordheim) ###

When the server shuts down it has to shut down the TCP listener as well. Unfortunately this listener is blocked and waiting for an incoming connection all the time. Except for right after having received a connection when it sets up a new thread to handle the new connection! As a poorly thought-through solution we then mark the server as 'shutting down'. This means all currently connected TCP clients will get a message that it's shutting down and disconnects, but it will also stop the listener from starting to listen again if a connection is received. Meaning that to shut down the TCP server as fast as possible you need someone to connect while it is waiting to shut down. If this doesn't happen the listener is forcibly closed after 10 seconds.

Spending more time in the Asio documentation may reveal some cleaner way of doing it, but this is what it does now.

stopTcpServer (the function described above):

[https://bitbucket.org/vrterra/irrlicht/src/master/src/Server/Server.cpp#cl-112](https://bitbucket.org/vrterra/irrlicht/src/master/src/Server/Server.cpp#cl-112)

TcpServer::stop (called by stopTcpServer, nothing wrong here though):

[https://bitbucket.org/vrterra/irrlicht/src/master/src/Server/Server.cpp#cl-233](https://bitbucket.org/vrterra/irrlicht/src/master/src/Server/Server.cpp#cl-233)

Where the listener lives (relevant because it shows the logic the listen-thread deals with):

[https://bitbucket.org/vrterra/irrlicht/src/master/src/Server/Server.cpp#cl-502](https://bitbucket.org/vrterra/irrlicht/src/master/src/Server/Server.cpp#cl-502)

### I need to test this but I don't have a kinect... (Mårten Nordheim) ###

In order to test shaders and networking without having a kinect connected we wrote a dead-simple function which randomly increases points in the terrain. It works but for a more accurate simulation a recording of the kinect-feed would have been a better choice.

[https://bitbucket.org/vrterra/irrlicht/src/master/src/NoKinect/main.cpp#cl-11](https://bitbucket.org/vrterra/irrlicht/src/master/src/NoKinect/main.cpp#cl-11)

### Current marker setup (Mårten Nordheim) ###

Currently the system with markers is a bit hacky. It isn't so much "one-time use" as it is "in need of refactoring". They're GUI elements and as such they get drawn when the GUI is drawn in the main loop (though this should be sufficient and will ensure they always stay on top.) The biggest issue here is that the current setup doesn't allow you to create markers with different textures without refactoring it.

Initializing markers and loading an image for the markers:

[https://bitbucket.org/vrterra/irrlicht/src/master\src\Base\Projector.cpp#cl-16](https://bitbucket.org/vrterra/irrlicht/src/master\src\Base\Projector.cpp#cl-16)

Updating the markers (and adding in new ones):

[https://bitbucket.org/vrterra/irrlicht/src/master\src\Base\Projector.cpp#cl-179](https://bitbucket.org/vrterra/irrlicht/src/master\src\Base\Projector.cpp#cl-179)

## Code Reviews ##

Most of our code reviews consisted of adding the other as a reviewer to a pull request. Then that person looked through the other's code in the pull request and pointed out things that seemed bad/good and asked questions as to why some things were like that.

We also had a few issues that we could not solve and then ended up pair programming to solve the issue.

As mentioned at the start of this document we detailed some refactoring which was carried out in the bachelor's report, namely creating subclasses of our main class 'Base' to separate the logic of the different programs that we were building.

## What We Would Have Done Differently ##

A few things that we would have done differently if we had our current knowledge at the beginning of the project.


### Engine ###
If we'd have known how easy it was to get started with an engine, rather than creating our own from scratch, we would have saved a lot of time not trying to set up our own engine, and would have spent more time looking for an engine in the beginning rather than look at a few alternatives and then conclude with the fact that we had to do our own (which we didn't since we ended up using Irrlicht.)


### Different approach to networking ###

The way it is now we have both ENet and TCP in our networking. Initially we (/I (Mårten)) chose ENet as a learning experience as I was curious to work with a 'tried and tested' networking library made specifically for games as opposed to writing layers on top of UDP ourselves. A lot of time was spent on this and later on we added in a TCP server to let others access the server without having to implement ENet's layer in their language of choice.

Using TCP would have had other implications for the architecture (the client should've ran in a separate thread as opposed to how it is structured with ENet) but this would be negligible compared to the time we could have saved by only doing TCP.

### Scope ###

We clearly overscoped the project. If we had known then what we know now we would have likely cut out the VR part as a part of the project plan, and only focused on making an API for the sandbox. We would then also have saved the time which we spent on the free-flying first-person application which lets you fly around the terrain which is read by the sandbox. The sandbox would then have been further towards a complete state when the project ended.

## A Note About Semantic Commits ##

While Taiga (which we used for US and issue tracking) does support semantic commits it did not work consistently (or maybe it was case-sensitive..) and, as mentioned in the report, we felt the syntax (ie. "TG-REF #STATUS-slug" where "REF" is the issue number and "STATUS-slug" is the 'slug'-name of a status, e.g. "in-progress", "ready-for-test", "closed") was inconvenient and difficult to remember off the top of our heads (fun fact: while writing it in this note I wrote it wrong), especially when compared to github and bitbucket where you can simply use a message like: "fixes #REF"

## Code Convention ##

We had a simply coding style written up. It can be found in Appendix D of the report as "Code Conventions". It is by no means an exhaustive code convention but addresses formatting and style of code. It does not mandate usage of 'X' over 'Y' or disallow usage of 'Z' unlike some code conventions do.

Additionally we set up a .clang-format file (located in the root of the repo) to automate formatting of our code to easily maintain consistency.